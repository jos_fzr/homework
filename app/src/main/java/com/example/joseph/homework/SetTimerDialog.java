package com.example.joseph.homework;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Joseph on 11/7/16.
 */

public class SetTimerDialog extends Dialog {

    public interface SetTimerDialogListener {
        void onSetClicked(long time);
    }

    private EditText mText;
    private Button mSetButton;
    private SetTimerDialogListener mListener;

    public static SetTimerDialog newInstance(Context context, SetTimerDialogListener listener){
        SetTimerDialog dlg = new SetTimerDialog(context);
        dlg.mListener = listener;
        return dlg;
    }

    public SetTimerDialog(Context context) {
        super(context);
    }

    public SetTimerDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    protected SetTimerDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timer_dialog);

        mText = (EditText) findViewById(R.id.time_edit_text);
        mSetButton = (Button) findViewById(R.id.set_time_button);

        mSetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mListener != null){
                    long l = Long.parseLong(mText.getText().toString());
                    if(l > 0) mListener.onSetClicked(l);
                }
                dismiss();
            }
        });
    }
}
