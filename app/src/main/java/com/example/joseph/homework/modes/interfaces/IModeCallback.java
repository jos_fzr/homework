package com.example.joseph.homework.modes.interfaces;

/**
 * Created by Joseph on 11/7/16.
 */

public interface IModeCallback {
    void onEvent(String textToShow);
    void alert(String text);
    void onStarted();
    void onStopped();
}
