package com.example.joseph.homework;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.joseph.homework.modes.GPSMode;
import com.example.joseph.homework.modes.interfaces.IDataViewBridge;
import com.example.joseph.homework.modes.StopWatchMode;
import com.example.joseph.homework.modes.TimerMode;
import com.example.joseph.homework.modes.interfaces.IModeCallback;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements IModeCallback, SetTimerDialog.SetTimerDialogListener {

    private Mode mCurrentMode;
    private Button mSetButton, mStartButton, mStopButon, mModeButton;
    private TextView mModeTextView, mInfoTextView;

    private HashMap<Integer, IDataViewBridge> mModes = new HashMap<>(3);
    private static final int REQUEST_CODE = 7946;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSetButton = (Button) findViewById(R.id.set_button);
        mStartButton = (Button) findViewById(R.id.start_button);
        mStopButon = (Button) findViewById(R.id.stop_button);
        mModeButton = (Button) findViewById(R.id.mode_button);

        mModeTextView = (TextView) findViewById(R.id.current_mode_text_view);
        mInfoTextView = (TextView) findViewById(R.id.info_text_view);

        setupListeners();

        setMode(Mode.STOP_WATCH);
        getViewBridgeFromMode(mCurrentMode);
    }

    private void setMode(Mode mode){
        mCurrentMode = mode;
        mModeTextView.setText("Current mode: " + mode.getConvenientName());
        IDataViewBridge bridge = getViewBridgeFromMode(mCurrentMode);
        if (bridge.getStatusInfo() != null)
            mInfoTextView.setText(bridge.getStatusInfo());

        mStartButton.setEnabled(!bridge.isStarted());
        mStopButon.setEnabled(bridge.isStarted());
    }

    private IDataViewBridge getViewBridgeFromMode(Mode mode){
        IDataViewBridge bridge = mModes.get(mode.ordinal());
        if (bridge == null) {
            if (mode == Mode.STOP_WATCH) bridge = new StopWatchMode();
            else if (mode == Mode.TIMER) bridge = new TimerMode();
            else bridge = new GPSMode(this);

            mModes.put(mode.ordinal(), bridge);
        }

        resetListeners();
        bridge.setListener(this);
        return bridge;
    }

    private void resetListeners(){
        for(IDataViewBridge bridge : mModes.values()){
            bridge.setListener(null);
        }
    }

    private void setupListeners() {
        mSetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentMode == Mode.TIMER){
                    SetTimerDialog.newInstance(MainActivity.this, MainActivity.this).show();
                } else {
                    getViewBridgeFromMode(mCurrentMode).set(0);
                }
            }
        });

        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentMode == Mode.GPS) {
                    if (ActivityCompat.checkSelfPermission(MainActivity.this,
                            Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(MainActivity.this,
                            Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[] { Manifest.permission.ACCESS_COARSE_LOCATION,
                                        Manifest.permission.ACCESS_FINE_LOCATION },
                                REQUEST_CODE);

                        return;
                    }
                }
                getViewBridgeFromMode(mCurrentMode).start();
            }
        });

        mStopButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getViewBridgeFromMode(mCurrentMode).stop();
            }
        });

        mModeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentMode == Mode.STOP_WATCH)
                    setMode(Mode.TIMER);
                else if (mCurrentMode == Mode.TIMER)
                    setMode(Mode.GPS);
                else if (mCurrentMode == Mode.GPS)
                    setMode(Mode.STOP_WATCH);
            }
        });
    }

    @Override
    public void onEvent(String textToShow) {
        mInfoTextView.setText(textToShow);
    }

    @Override
    public void alert(String text) {
        AlertDialog.Builder dlg = new AlertDialog.Builder(this);
        dlg.setCancelable(true);
        dlg.setTitle("Alert");
        dlg.setMessage("Set clicked in: " + text);
        dlg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dlg.create().show();
    }

    @Override
    public void onStarted() {
        mStartButton.setEnabled(false);
        mStopButon.setEnabled(true);
    }

    @Override
    public void onStopped() {
        mStartButton.setEnabled(true);
        mStopButon.setEnabled(false);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CODE && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (mCurrentMode == Mode.GPS) {
                getViewBridgeFromMode(mCurrentMode).start();
            }
        }

    }

    @Override
    public void onSetClicked(long time) {
        if(mCurrentMode == Mode.TIMER){
            getViewBridgeFromMode(mCurrentMode).set(time);
        }
    }
}
