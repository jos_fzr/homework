package com.example.joseph.homework.modes;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.example.joseph.homework.modes.interfaces.IDataViewBridge;
import com.example.joseph.homework.modes.interfaces.IModeCallback;

/**
 * Created by Joseph on 11/7/16.
 */

public class GPSMode implements IDataViewBridge {

    private boolean isStarted = false;
    private IModeCallback mCallback;
    private LocationManager mLocationManager;
    private LocationListener mLocationListener;
    private String mCurrentStateInfo = "Location not found";
    private Context mContext;



    public GPSMode(Context context) {
        mContext = context;
        mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                mCurrentStateInfo = "Current latitude: " + String.valueOf(location.getLatitude()) + ", longitude " + String.valueOf(location.getLongitude());
                isStarted = false;
                if (mCallback != null) {
                    mCallback.onEvent(mCurrentStateInfo);
                    mCallback.onStopped();
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
    }

    @Override
    public void start() {
        isStarted = true;
        if(mCallback != null){
            mCallback.onStarted();
        }
        try {
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 1, mLocationListener);
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, mLocationListener);
        } catch (SecurityException e){
            isStarted = false;
            if(mCallback != null){
                mCallback.onStopped();
            }
            Log.e("GPSMode", e.getMessage());
        }
    }

    @Override
    public void stop() {
        isStarted = false;
        if(mCallback != null){
            mCallback.onStopped();
        }
        try {
            mLocationManager.removeUpdates(mLocationListener);
        } catch (SecurityException e){
            Log.e("GPSMode", e.getMessage());
        }

    }

    @Override
    public void set(long time) {
        if (mCallback != null) {
            mCallback.alert("GPS Mode");
        }
    }

    @Override
    public String getStatusInfo() {
        return mCurrentStateInfo;
    }

    @Override
    public void setListener(IModeCallback callback) {
        mCallback = callback;
    }

    @Override
    public boolean isStarted() {
        return isStarted;
    }
}
